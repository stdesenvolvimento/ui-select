uis.filter('multipleFilter', ['$filter', function($filter) {

  function compareText(item, query) {
    return item.toString().toLowerCase().indexOf(query.toString().toLowerCase()) !== -1;
  }

  function doesItMatch(item, props) {
    if(!item) return false;
    var keys = Object.keys(props);
    for (var i = 0; i < keys.length; i++) {
      var prop = keys[i];
      var currentItem = item[prop];
      var query = props[prop];

      if (!angular.isObject(query) && compareText(currentItem, query)) {
        return true;
      }

      if (angular.isObject(query)) {
        if (doesItMatch(currentItem, query)) {
          return true;
        }
      }
    }
  }

  return function(items, props) {
    if (!angular.isArray(items)) {
      return items;
    }

    var out = [];

    items.forEach(function(item) {
      if (doesItMatch(item, props)) {
        out.push(item);
      }
    });

    return out;
  };
}]);

